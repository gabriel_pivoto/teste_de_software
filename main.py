from src.calculator import Calculator
from src.operations.sum import SumOperation
from src.operations.sub import SubOperation

calculator = Calculator(SumOperation(), SubOperation())

print(calculator.addition(123, 456, True))
print(calculator.subtraction(123, 456, True))

